package fr.LigerotRobert.recytuto;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class AddItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Button valider = (Button) findViewById(R.id.buttonAjouter);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextNomItem = (EditText)findViewById(R.id.nomItem);
                String nomItem = editTextNomItem.getText().toString();
                TodoItem.Tags tag;
                if (((RadioButton) findViewById(R.id.rbFaible)).isChecked())
                    tag = TodoItem.Tags.Faible;
                else if (((RadioButton) findViewById(R.id.rbNormal)).isChecked())
                    tag = TodoItem.Tags.Normal;
                else
                    tag = TodoItem.Tags.Important;

                TodoItem item = new TodoItem(tag, nomItem);
                TodoDbHelper.addItem(item, getApplicationContext());

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
